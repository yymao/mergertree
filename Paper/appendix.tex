\documentclass[useAMS,usenatbib,usegraphicx,onecolumn]{mn2e}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage[latin1]{inputenc}
\usepackage{times} 
\usepackage{color}     
\usepackage{graphicx}

\usepackage[normalem]{ulem} 
\usepackage{epstopdf}
\usepackage{epsfig}

\newcommand{\hdf}{HDF5}

\title[Merger Tree Data Format]
{Sussing Merger Trees: A proposed Merger Tree data format}
\author[Sussing Merger Trees Workshop participants]
{Peter~A.~Thomas et al.} 

\setcounter{page}{6}
\begin{document}

\appendix

\section{Example {\sc Python} codes}
\label{sec:python}

In this appendix we present some example {\sc Python} codes to search merger
trees and to read/write them in \hdf\ format.  These routines can all be
downloaded from {\tt https://bitbucket.org/ProfPAThomas/mergertree/}.

\subsection{Tree-search algorithm}
\label{sec:tree}
We present below an example tree class with methods for generating iterators
that can perform a combined (default behaviour), spatial, or temporal tree
search. 
\begin{verbatim}
class Tree:
    """Tree class for dealing with combined temporal and merger trees"""

    # Constructor: creates an empty node.
    def __init__(self,node):
        self.node=node

    # Load pointer arrays
    def load(self,firstSubHalo,neighbour,firstProgenitor,nextSibling):
        self.firstSubHalo=firstSubHalo
        self.neighbour=neighbour
        self.firstProgenitor=firstProgenitor
        self.nextSibling=nextSibling

    # Default iterator that takes no arguments:
    # Note the asymmetry between spatial and temporal: because there is more
    # than one way to reach a node, can only iterate over one of them.
    # Because of the nature of the trees, space has to be the outer loop.
    def __next__(self):
        yield self.node
        yield from self.nextSpatial()
        yield from self.nextTemporal('temporal')
    # More general iterator method with optional argument specifying tree type:
    def next(self,treeType=None):
        yield self.node
        if treeType==None:
            yield from self.nextSpatial()
            yield from self.nextTemporal('temporal')
        elif treeType=='spatial': yield from self.nextSpatial('spatial')
        elif treeType=='temporal': yield from self.nextTemporal('temporal')
        else: raise valueError('Invalid treeType for tree')
    # Spatial tree search
    def nextSpatial(self,treeType=None):
        if self.firstSubHalo != None: yield from self.firstSubHalo.next(treeType)
        if self.neighbour != None: yield from self.neighbour.next(treeType)
    # Temporal tree search
    def nextTemporal(self,treeType=None):
        if self.firstProgenitor != None: yield from self.firstProgenitor.next(treeType)
        if self.nextSibling != None: yield from self.nextSibling.next(treeType)

    # Printing
    def __str__(self):
        print ('node,firstSubHalo,neighbour,firstProgenitor,nextSibling=',
               self.node,self.firstSubHalo,self.neighbour,self.firstProgenitor,self.nextSibling)

    # Tracing
    def trace(self):
        for halo in self.next(): print(halo)
\end{verbatim}

\subsection{Merger tree class and associated methods}

\begin{verbatim}
""" Merger tree class to simplify some i/o functions for merger trees."""

import h5py

class MergerTree:
    
    def __init__(self, file=None, mode='r'):
        """Initialise the class, opening the file with the mode given"""
        if file: 
            self.open(file, mode)
        else:
            print('mt usage: mt(file, mode=mode)')

    # Return set of attributes associated with object
    def attrs(self, object):
        return self.fid[object].attrs.items()

    # Close the HDF5 file
    def close(self):
        self.fid.close()

    # Open the HDF5 file
    def open(self, file, mode):
        self.fid = h5py.File(file, mode)

    # Return a handle to the MergerTree group
    def read_mergertree(self):
        return self.fid["MergerTree"]

    # Return a handle to the Snapshot table
    def read_snaptable(self):
        return self.fid["Snapshots/Snap"]
\end{verbatim}

\subsection{To read a tree written in the default format}
\label{sec:readtree}
\begin{verbatim}
# Example of how to read in a merger tree in HDF5 format

import sys
import mt   # Import MergerTree class

#-------------------------------------------------------------------------------

# Open file
if len(sys.argv) != 2: sys.exit("Usage: " + sys.argv[0] + " filename.hd5")
mtdata=mt.MergerTree(sys.argv[1], "r")
for (name,value) in mtdata.attrs('/'): print(name,value)

# Read in snap table
for (name,value) in mtdata.attrs("Snapshots"): print(name,value)
snap=mtdata.read_snaptable()
print('Snap =')
print('{0:>8} {1:>6} {2:>6} {3:>6}'.format(*snap.dtype.names))
for row in range(len(snap)): print('{0:>8} {1:>6.3f} {2:>6.3f} {3:>6.3f}'.format(*snap[row]))
del snap

# Read merger tree from HDF file
for (name,value) in mtdata.attrs("MergerTree"): print(name,value)
tree=mtdata.read_mergertree()
print('Tree =')
for dset in tree: 
    print(dset,end=' ')
print()
for ntree in range(min(10,len(tree['HaloID']))): 
    for prop in tree: 
        print(tree[prop][ntree],end=' ')
    print()
del tree

#Close file
mtdata.close()
\end{verbatim}

\subsection{To save a tree in the default format}
\begin{verbatim}
""" Example of how to write a merger tree in HDF5 format.
    This example needs some data which it reads in from an existing HDF file,
    which makes this whole exercise a bit pointless, but pretend that the data
    was created some other way and now needs to be written out.
    This is not complete, but it gives the general idea. """

import sys
import numpy as np
import mt  # MergerTree class

infile='data/test.hdf5'
outfile='data/dumptest.hdf5'

#-------------------------------------------------------------------------------

# For the purposes of this example, we first have to read in some data.
# Actually, the following creates pointers to the data, to be read below.

mt_in=mt.MergerTree(infile, "r")
attributes = mt_in.attrs('/')

# Read in snap table
snap_in=mt_in.read_snaptable()
snap_attributes=mt_in.attrs("Snapshots")

# Read merger tree.
tree_in=mt_in.read_mergertree()
tree_attributes=mt_in.attrs("MergerTree")

#-------------------------------------------------------------------------------

# Now we are going to create a new HDF5 file to write out the data
mt_out=mt.MergerTree(outfile,"w")
print('Output file =',mt_out.fid)

# Write out the top-level attributes
print('In group',mt_out.fid.name,'attributes are:')
for (name,value) in attributes: 
    print('  ',name,value)
    mt_out.fid.attrs.modify(name,value)

# Create MergerTree directory and set attributes
tree=mt_out.fid.create_group("MergerTree")
print('In group',tree.name,'attributes are:')
for (name,value) in tree_attributes: 
    print('name, value =',name,value)
    tree.attrs.modify(name,value)

# Write out merger tree datasets
print('MergerTree datasets:')
for dset in tree_in:
    # extract name, dtype and data from input dataset
    name=tree_in[dset].name
    dtype=tree_in[dset].dtype
    data=tree_in[dset][()]
    attrs=tree_in[dset].attrs.items()
    # and now recreate in the output file
    dset=tree.create_dataset(name=name,dtype=dtype,data=data)
    print('  ',name)
    print('    ',dset[0:9])
    # and set attributes
    for (name,value) in attrs: 
        print('    ',name,value)
        dset.attrs.modify(name,value)

# Create Snapshot directory and set attributes
snap=mt_out.fid.create_group("Snapshots")
print('In group',snap.name,'attributes are:')
for (name,value) in snap_attributes: 
    print('  ',name,value)
    snap.attrs.modify(name,value)

# Write out Snap table
# Tables require use of a complex dtype.  Fortuntately, that's already set up
# us here in the input file.
# Extract data and dtype from input file
name=snap_in.name
dtype=snap_in.dtype
nsnap=len(snap_in)
data=np.empty(nsnap,dtype=dtype)
for irow in range(nsnap): data[irow]=snap_in[irow]
# and recreate in output file
dset=tree.create_dataset(name=name,dtype=dtype,data=data)
print('Snap table:')
print('  name =',dset.name)
print('  dtype =',dset.dtype)
print('  data =',dset[:])

# The output file is not yet a complete copy of the input, but hopefully
# that's enough to illustrate how to create the required structures.

#-----------------------------------------------------------------------

# Close the files
mt_in.close()
mt_out.close()
\end{verbatim}


\end{document}
