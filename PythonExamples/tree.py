from __future__ import print_function

class Tree:
    """Tree class for dealing with combined temporal and merger trees"""

    # Constructor: creates an empty tree node.
    def __init__(self,node):
        self.node=node

    # Load pointer arrays.
    def load(self,firstSubHalo,neighbour,firstProgenitor,nextSibling):
        self.firstSubHalo=firstSubHalo
        self.neighbour=neighbour
        self.firstProgenitor=firstProgenitor
        self.nextSibling=nextSibling

    # Default iterator that takes no arguments:
    # Note the asymmetry between spatial and temporal: because there is more
    # than one way to reach a node, can only iterate over one of them.
    # Because of the nature of the trees, space has to be the outer loop.
    def __next__(self):
        yield self.node
        for __ in self.nextSpatial():
            yield __
        for __ in self.nextTemporal('temporal'):
            yield __
    # More general iterator method with optional argument specifying tree type:
    def next(self,treeType=None):
        yield self.node
        if treeType is None:
            for __ in self.nextSpatial():
                yield __
            for __ in self.nextTemporal('temporal'):
                yield __
        elif treeType=='spatial':
            for __ in self.nextSpatial('spatial'):
                yield __
        elif treeType=='temporal':
            for __ in self.nextTemporal('temporal'):
                yield __
        else:
            raise valueError('Invalid treeType for tree')
    # Spatial tree search
    def nextSpatial(self,treeType=None):
        if self.firstSubHalo is not None:
            for __ in self.firstSubHalo.next(treeType):
                yield __
        if self.neighbour is not None:
            for __ in self.neighbour.next(treeType):
                yield __
        #raise StopIteration()
    # Temporal tree search
    def nextTemporal(self,treeType=None):
        if self.firstProgenitor is not None:
            for __ in self.firstProgenitor.next(treeType):
                yield __
        if self.nextSibling is not None:
            for __ in self.nextSibling.next(treeType):
                yield __
        #raise StopIteration()

    # Printing
    def __str__(self):
        print ('node,firstSubHalo,neighbour,firstProgenitor,nextSibling=',
               self.node,self.firstSubHalo,self.neighbour,self.firstProgenitor,self.nextSibling)

    # Tracing
    def trace(self):
        for halo in self.next():
            print(halo)
