# Example of how to read in a merger tree in HDF5 format

#-------------------------------------------------------------------------------
# Imports
from __future__ import print_function
import sys

# The merger tree module contains data structures, methods and global variables
import mt

#-------------------------------------------------------------------------------

# Open file
if len(sys.argv) != 2: sys.exit("Usage: " + sys.argv[0] + " filename.hd5")
mtdata=mt.MergerTree(sys.argv[1], "r")
for (name,value) in mtdata.attrs('/'): print(name,value)

# Read in snap table
for (name,value) in mtdata.attrs("Snapshots"): print(name,value)
snap=mtdata.read_snaptable()
print('Snap =')
print('{0:>8} {1:>6} {2:>6} {3:>6}'.format(*snap.dtype.names))
for row in range(len(snap)): print('{0:>8} {1:>6.3f} {2:>6.3f} {3:>6.3f}'.format(*snap[row]))
del snap

# Read merger tree from HDF file
for (name,value) in mtdata.attrs("MergerTree"): print(name,value)
tree=mtdata.read_mergertree()
print('Tree =')
for dset in tree: 
    print(dset,end=' ')
print()
for ntree in range(min(10,len(tree['HaloID']))): 
    for prop in tree: 
        print(tree[prop][ntree],end=' ')
    print()
del tree

#Close file
mtdata.close()

