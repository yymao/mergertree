// HDF high-level routines

#include "hdf5.h"

//--------------------------------------------------------

// Finalises the HDF5 libraries: flushes buffers and frees resources.
// Optional in C, but recommended.

int mt_h5close(void) {

  return (int)H5close();

}

//--------------------------------------------------------

// Initialises the HDF5 libraries.  Optional in C, but recommended.

int mt_h5open(void) {

  return (int)H5open();

}
