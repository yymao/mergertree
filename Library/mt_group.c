#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "hdf5.h"
#include "hdf5_hl.h"

//------------------------------------------------------------

// Close a group and free resources

int mt_gclose(hid_t gid) {

  return (int)H5Gclose(gid);

}

//------------------------------------------------------------

// Open existing or create new group

hid_t mt_gopen(hid_t hid, char *name) {

  hid_t gid;
  htri_t exists;

  exists=H5LTpath_valid (hid, name, false);

  if (exists)
    gid=H5Gopen(hid, name, H5P_DEFAULT);
  else
    gid=H5Gcreate(hid, name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  return gid;

}
