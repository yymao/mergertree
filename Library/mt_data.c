// Routines to Read and write arrays (datasets)

#include "hdf5.h"
#include "hdf5_hl.h"
#include <stdlib.h>
#include <string.h>

//---------------------------------------------

int mt_dread(hid_t hid, const char *name, const char *type, void *data) {

  // Inputs:
  //    hid - identifier of group
  //    name - name of dataset (ie array)
  //    type - type of data (must be in list below)
  // Outputs:
  //    data - address of array to hold data

  hid_t tid;

  // No native types with guaranteed length
  if (strcmp(type,"int64")==0)
    tid = H5T_INTEL_I64;
  else if (strcmp(type,"int32")==0)
    tid = H5T_INTEL_I32;
  else if (strcmp(type,"float")==0)
    tid = H5T_INTEL_F32;
  else if (strcmp(type,"double")==0)
    tid = H5T_INTEL_F64;
  else {
    printf("***mt_dread: undefined datatype %s***\n",type);
    return -1;
  }

  return (int)H5LTread_dataset(hid, name, tid, data);

}

//---------------------------------------------

int mt_dmake(hid_t hid, const char *name, hsize_t nobj, const char *type, void *data) {

  // Inputs:
  //    hid - identifier of group
  //    name - name of dataset (ie array)
  //    nobj - number of objects to write
  //    type - type of data (must be in list below)
  // Outputs:
  //    data - address of array to hold data

  hid_t tid;
  hsize_t ndim[1];

  ndim[0]=nobj;

  // No native types with guaranteed length so use intel representation
  if (strcmp(type,"int64")==0)
    tid = H5T_INTEL_I64;
  else if (strcmp(type,"int32")==0)
    tid = H5T_INTEL_I32;
  else if (strcmp(type,"float")==0)
    tid = H5T_INTEL_F32;
  else if (strcmp(type,"double")==0)
    tid = H5T_INTEL_F64;
  else {
    printf("***mt_dmake: undefined datatype %s***\n",type);
    return -1;
  }

  return (int)H5LTmake_dataset(hid, name, 1, ndim, tid, data);

}
