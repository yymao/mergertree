/*
  Example of how to write a merger tree in HDF5 format.
  This example needs some data which it reads in from an existing HDF file,
  which makes this whole exercise a bit pointless, but pretend that the data
  was created some other way and now needs to be written out.
  This is not complete, but it gives the general idea.

  The idea is to hide underlying HDF libraries from the user */

#include <string.h>

// Load function interfaces
#include "mt.h"

// This defines the merger tree structures as per the standard
// and sets up some global variables
#include "mt_data.h"

int main(void) {

  char *infile="data/test.hdf5";
  char *outfile="data/dumptest_C.hdf5";
  char description[CSIZE];

  /* Create some mock data to write */
  read_mock_data(infile);

  // Open data file
  // NOTE: Cannot replace description with a constant string
  strcpy(description,"Test data set created by dumptree.c");
  mt_open(outfile,"wo",description);

  // Write top-level attribhutes
  mt_aset_float(mt_fid,".","BoxsizeMpc",box);
  mt_aset_float(mt_fid,".","H100",h100);
  mt_aset_float(mt_fid,".","OmegaBaryon",omega_baryon);
  mt_aset_float(mt_fid,".","OmegaCDM",omega_cdm);
  mt_aset_float(mt_fid,".","OmegaLambda",omega_lambda);
  mt_aset_float(mt_fid,".","Sigma8",sigma8);
  mt_aset_string(mt_fid,".","Title",title);

  // Call a general routine to write the SnapTable out to the hdf file
  mt_dump_snaptable();

  // Call a general routine to write the MergerTree out to the HDF file
  mt_dump_mergertree();

  // Close open file
  mt_close();

  // Free resources (not strictly necessary)
  mt_tidy();

  return 0;

}
