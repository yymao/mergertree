// function prototypes

#include "hdf5.h"
//#include "mt_f90name.h"

// Local to this CExamples directory
int create_mock_data(void);
int mt_close(void);
int mt_dump_mergertree(void);
int mt_dump_snaptable(void);
int mt_open(char *file, char *ioflags, char *description);
int mt_read_mergertree(void);
int mt_read_snaptable(void);
int mt_tidy(void);
int read_mock_data(char *infile);

// Part of the central C library
int mt_aget_float(hid_t hid, char *name, char *attr, float *value);
int mt_aset_float(hid_t hid, char *name, char *attr, float value);
int mt_aget_int(hid_t hid, char *name, char *attr, int *value);
int mt_aset_int(hid_t hid, char *name, char *attr, int value);
int mt_aget_string(hid_t hid, char *name, char *attr, char *value);
int mt_aset_string(hid_t hid, char *name, char *attr, char *value);
int mt_dmake(hid_t hid, const char *name, hsize_t nobj, const char *type, void *data);
int mt_dread(hid_t hid, const char *name, const char *type, void *data);
hid_t mt_fopen(char *file, char *ioflags, char *description);
int mt_fclose(hid_t fid);
hid_t mt_gopen(hid_t hid, char *name);
int mt_gclose(hid_t gid);
int mt_h5close();
int mt_h5open();
int mt_tmake(hid_t gid, const char *name, hsize_t nfields, const char *field_names[],
	     const char field_types[], hsize_t nrecords, hsize_t chunksize,
	     void *table);
int mt_tread(hid_t gid, const char *name, hsize_t nfields, const char *field_names,
	     const char field_types[], hsize_t nrecords, void *table);
hsize_t mt_treadinfo(hid_t gid, const char *name, hsize_t *nfields);
