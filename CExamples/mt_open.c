// Starts HDF5 and opens a file

#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

// HDF5
#include "hdf5.h"
// Function prototypes
#include "mt.h"
// Global data
#include "mt_data.h"

int mt_open(char *file, char *ioflags, char *comment) {

  /* Inputs: 
     file - file name
     ioflags - read/write string;
        "r" file must exist and will be opened readonly
	"rw" file must exist and will be opened for writing
	"wo" file will be created, over-writing any existing file
	"w" file will be created, will fail if already exists
     description -  description of dataset */

  // Start HDF5
  mt_h5open();
  
  // Open file and read/write comment
  mt_fid=mt_fopen(file,ioflags,comment);

  return 0;

}

