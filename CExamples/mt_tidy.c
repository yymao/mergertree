// Free open resources (not strictly necessary)

#include <stdlib.h>
#include "mt_data.h"

int mt_tidy(void) {

  free(Snap);
  free(mtHaloID);
  free(mtSnapshot);
  free(mtDensity);
  free(mtMass);
  free(mtFirstSubHaloIndex);
  free(mtNeighbourIndex);
  free(mtHostHaloIndex);
  free(mtFirstProgenitorIndex);
  free(mtNextSiblingIndex);
  free(mtDescendantIndex);

  return 0;

}

