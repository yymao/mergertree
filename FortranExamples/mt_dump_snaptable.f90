subroutine mt_dump_snaptable()

! Dumps a Snapshot Table to an HDF file in the official mt format

! ToDo: add ability to dump only minimal number of fields in a SnapTable
!       (currently has SnapID, a, z, t)
  
  use hdf5
  use iso_c_binding
  use mt
  use mt_data

  implicit none

  integer :: ifield
  integer(hid_t) :: gid
  integer(hsize_t) :: chunk_size
  type(c_ptr), allocatable :: field_names(:)  ! To hold addresses of field_names

  ! First check to see whether the Snapshots group exists and either open it or make it
  gid=mt_gopen(mt_fid,mt_str_f2c('Snapshots'))

  ! Write the NSnap attribute
  call mt_aset_int(gid,mt_str_f2c('.'),mt_str_f2c('NSnap'),int(NSnap))

  ! Dump the Snap Table
  ! Convert the field names into C-format
  forall(ifield=1:snaptable_nfields) &
       & snaptable_field_names(ifield)= mt_str_f2c(snaptable_field_names(ifield))
  ! C is expecting an array that holds the location of each string
  allocate(field_names(snaptable_nfields))
  forall(ifield=1:snaptable_nfields) &
       & field_names(ifield)=c_loc(snaptable_field_names(ifield))
  ! For this short table, set the chunk-size (which I think is in units of records)
  ! equal to the number of records
  chunk_size=Nsnap
  ! Create the table.  I have left out parameters that seem redundant
  call mt_tmake(gid,mt_str_f2c('Snap'),&
       &snaptable_nfields,field_names,snaptable_field_types,NSnap,chunk_size,Snap)
  ! Deallocate field_names array
  deallocate(field_names)

  ! Dump the SnapProp table
  ! Convert the field names into C-format
  forall(ifield=1:snapprop_nfields) &
       & snapprop_field_names(ifield)= mt_str_f2c(snapprop_field_names(ifield))
  ! C is expecting an array that holds the location of each string
  allocate(field_names(snapprop_nfields))
  forall(ifield=1:snapprop_nfields) &
       & field_names(ifield)=c_loc(snapprop_field_names(ifield))
  ! Create the table
  call mt_tmake(gid,mt_str_f2c('SnapProp'),snapprop_nfields,field_names,&
       &snapprop_field_types,snaptable_nfields,snaptable_nfields,SnapProp)

  ! Now close the open group
  call mt_gclose(gid)

end subroutine mt_dump_snaptable
