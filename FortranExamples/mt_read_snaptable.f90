subroutine mt_read_snaptable()

! Reads a Snapshot Table from an HDF file in the official mt format

  use hdf5
  use iso_c_binding
  use mt
  use mt_data

  implicit none

  integer(hid_t) :: gid
  integer(hsize_t) :: nfields

  ! First check to see whether the Snapshots group exists and either open it or make it
  gid=mt_gopen(mt_fid,mt_str_f2c('Snapshots'))

  ! First the Snap table...

  ! Can't think of any simple way of reading the table in one go
  ! Read the table length
  NSnap=mt_treadinfo(gid,mt_str_f2c('Snap'),nfields)
  if (nfields < snaptable_nfields) stop '***mt_read_snaptable: nfields < snaptable_nfields***'
  if (NSnap <= 0) stop '***mt_read_snaptable: NSnap <= 0***'

  ! Allocate space for the table
  allocate(Snap(NSnap))

  ! Read in the table
  call mt_tread(gid,mt_str_f2c('Snap'),snaptable_nfields,&
       mt_str_f2c(snaptable_fieldnames),snaptable_field_types,&
       &NSnap,Snap)

  ! Next the SnapProp table...

  ! Can't think of any simple way of reading the table in one go
  ! Read the table length
  NSnapProp=mt_treadinfo(gid,mt_str_f2c('SnapProp'),nfields)
  if (nfields < snapprop_nfields) stop '***mt_read_snaptable: nfields < snaptable_nfields***'
  if (NSnapProp <= 0) stop '***mt_read_snapprop: NSnapProp <= 0***'

  ! Allocate space for the table
  allocate(SnapProp(NSnapProp))

  ! Read in the table
  call mt_tread(gid,mt_str_f2c('SnapProp'),snapprop_nfields,&
       mt_str_f2c(snapprop_fieldnames),snapprop_field_types,&
       &NSnapProp,SnapProp)

  ! Now close the open group
  call mt_gclose(gid)

end subroutine mt_read_snaptable
