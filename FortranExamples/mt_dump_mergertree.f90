subroutine mt_dump_mergertree()

! Dumps a MergerTree to an HDF file as a collection of arrays

! ToDo: add option to dump as a table instead
  
  use hdf5
  use iso_c_binding
  use mt
  use mt_data

  implicit none

  ! Abbreviations for c_strings needed in function calls
  character(c_char) :: dot*(csize), int64_t*(csize), int32_t*(csize)
  character(c_char) :: float_t*(csize), double_t*(csize)
  character(c_char) :: array*(csize), description*(csize), none*(csize), units*(csize)
  integer(hid_t) :: gid

  ! Make the abbreviations
  dot=mt_str_f2c('.')
  int64_t=mt_str_f2c('int64')
  int32_t=mt_str_f2c('int32')
  float_t=mt_str_f2c('float')
  double_t=mt_str_f2c('double')
  description=mt_str_f2c('Description')
  none=mt_str_f2c('None')
  units=mt_str_f2c('Units')

  ! First check to see whether the MergerTree group exists and either open it 
  ! or make it
  gid=mt_gopen(mt_fid,mt_str_f2c('MergerTree'))

  ! Write the attributes
  call mt_aset_int(gid, dot, mt_str_f2c('NHalo'), int(NHalo))
  call mt_aset_int(gid, dot, mt_str_f2c('HaloIndexOffset'), HaloIndexOffset)
  call mt_aset_int(gid, dot, mt_str_f2c('TableFlag'), TableFlag)

  ! Write the arrays
  ! HaloID
  array=mt_str_f2c('HaloID')
  call mt_dmake(gid, array, NHalo, int64_t, mtHaloID)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('ID in original halo catalogue'))
  call mt_aset_string(gid, array, units, none)
  ! Snapshot
  array=mt_str_f2c('Snapshot')
  call mt_dmake(gid, array, NHalo, int32_t, mtSnapshot)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Snapshot in which this halo resides'))
  call mt_aset_string(gid, array, units, none)
  ! Density
  array=mt_str_f2c('Density')
  call mt_dmake(gid, array, NHalo, float_t, mtDensity)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Overdensity of this halo relative to the critical density at that redshift'))
  call mt_aset_string(gid, array, units, none)
  ! Mass
  array=mt_str_f2c('Mass')
  call mt_dmake(gid, array, NHalo, float_t, mtMass)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Mass of halo'))
  call mt_aset_string(gid, array, units, mt_str_f2c('Msun'))
  ! FirstSubHalo
  array=mt_str_f2c('FirstSubHaloIndex')
  call mt_dmake(gid, array, NHalo, int32_t, mtFirstSubHaloIndex)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Pointer to FirstSubHalo'))
  call mt_aset_string(gid, array, units, none)
  ! Neighbour
  array=mt_str_f2c('NeighbourIndex')
  call mt_dmake(gid, array, NHalo, int32_t, mtNeighbourIndex)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Pointer to Neighbour'))
  call mt_aset_string(gid, array, units, none)
  ! HostHalo
  array=mt_str_f2c('HostHaloIndex')
  call mt_dmake(gid, array, NHalo, int32_t, mtHostHaloIndex)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Pointer to HostHalo'))
  call mt_aset_string(gid, array, units, none)
  ! FirstProgenitor
  array=mt_str_f2c('FirstProgenitorIndex')
  call mt_dmake(gid, array, NHalo, int32_t, mtFirstProgenitorIndex)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Pointer to FirstProgenitor'))
  call mt_aset_string(gid, array, units, none)
  ! NextSibling
  array=mt_str_f2c('NextSiblingIndex')
  call mt_dmake(gid, array, NHalo, int32_t, mtNextSiblingIndex)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Pointer to NextSibling'))
  call mt_aset_string(gid, array, units, none)
  ! Descendant
  array=mt_str_f2c('DescendantIndex')
  call mt_dmake(gid, array, NHalo, int32_t, mtDescendantIndex)
  call mt_aset_string(gid, array, description, &
       &mt_str_f2c('Pointer to Descendant'))
  call mt_aset_string(gid, array, units, none)

  ! Now close the open group
  call mt_gclose(gid)

end subroutine mt_dump_mergertree
